# amstradcpcemu


![](https://gitlab.com/openbsd98324/amstradcpcemu/-/raw/master/screenshot/BASIC1.1-Amstrad-Hello.png)


Example using a PI:



 /opt/retropie/emulators/retroarch/bin/retroarch -L /opt/retropie/libretrocores/lr-caprice32/cap32_libretro.so --config /opt/retropie/configs/amstradcpc/retroarch.cfg /home/pi/RetroPie/roms/amstradcpc/garfield.dsk --appendconfig /dev/shm/retroarch.cfg



 
